kind: autotools
description: GNU gcc Stage 2

depends:
- filename: dependencies/base-sdk.bst
  type: build
- filename: dependencies/config.bst
  type: build
- filename: binutils-stage1.bst
  type: build
- filename: glibc.bst
  type: build
- filename: linux-headers.bst
  type: build
- filename: libstdc++-stage1.bst
  type: build
- filename: cross-installation-links.bst
  type: build

variables:
  conf-link-args: |
    --enable-shared \
    --enable-static
  prefix: "%{tools}"
  lib: "lib"
  (?):
    - target_arch == "arm":
        conf-extra: |
          --with-mode=thumb \
          --with-fpu=vfpv3-d16 \
          --with-arch=armv7-a \
          --with-float=hard

  build-triplet: "$(sh /config/config.guess)"
  host-triplet: "$(sh /config/config.guess)"

  conf-local: |
    --target=%{triplet} \
    --enable-multiarch \
    --disable-multilib \
    --disable-bootstrap \
    --disable-nls \
    --with-sysroot=%{sysroot} \
    --enable-languages=c,c++,fortran \
    --enable-default-pie \
    --enable-default-ssp \
    --without-isl \
    --enable-deterministic-archives \
    %{conf-extra}

sources:
- kind: tar
  url: ftp://ftp.fu-berlin.de/unix/languages/gcc/releases/gcc-8.1.0/gcc-8.1.0.tar.xz
  ref: 1d1866f992626e61349a1ccd0b8d5253816222cdc13390dcfaa74b093aa2b153
- kind: tar
  url: http://ftp.gnu.org/gnu/gmp/gmp-6.1.2.tar.xz
  directory: gmp
  ref: 87b565e89a9a684fe4ebeeddb8399dce2599f9c9049854ca8c0dfbdea0e21912
- kind: tar
  url: http://ftp.gnu.org/gnu/mpfr/mpfr-4.0.1.tar.xz
  directory: mpfr
  ref: 67874a60826303ee2fb6affc6dc0ddd3e749e9bfcb4c8655e3953d0458a6e16e
- kind: tar
  url: http://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz
  directory: mpc
  ref: 6985c538143c1208dcb1ac42cedad6ff52e267b47e5f970183a3e75125b43c2e
- kind: patch
  path: patches/gcc-multiarch-os-dir.patch
