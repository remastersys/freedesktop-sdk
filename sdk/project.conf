name: base-sdk

format-version: 7

aliases:
  freedesktop: https://anongit.freedesktop.org/git/

element-path: elements

variables:
  builddir: bst_build_dir
  conf-deterministic: |
    --enable-deterministic-archives
  conf-link-args: |
    --enable-shared \
    --disable-static
  conf-host: |
    --host=%{host-triplet}
  conf-build: |
    --build=%{build-triplet}
  host-triplet: "%{triplet}"
  build-triplet: "%{triplet}"
  sbindir: "%{bindir}"
  sysconfdir: "%{prefix}/etc"
  localstatedir: "%{prefix}/var"
  branch: "unstable"
  lib: "lib/%{gcc_triplet}"
  indep-libdir: "%{prefix}/lib"
  debugdir: "%{indep-libdir}/debug"
  gcc_triplet: "%{gcc_arch}-linux-%{abi}"
  triplet: "%{arch}-unknown-linux-%{abi}"
  gcc_arch: "%{arch}"
  abi: "gnu"
  (?):
    - target_arch == "i586":
        gcc_arch: "i386"
    - target_arch == "arm":
        abi: "gnueabihf"

  strip-binaries: |
    find "%{install-root}" -type f \
      '(' -perm -111 -o -name '*.so*' \
          -o -name '*.cmxs' -o -name '*.node' ')' \
      -exec sh -ec \
      'read -n4 hdr <"$1" # check for elf header
       if [ "$hdr" != "$(printf \\x7fELF)" ]; then
           exit 0
       fi
       if objdump -j .gnu_debuglink -s "$1" &>/dev/null; then
         exit 0
       fi
       case "$1" in
         "%{install-root}%{debugdir}/"*)
           exit 0
           ;;
         *)
           ;;
       esac
       realpath="$(realpath -s --relative-to="%{install-root}" "$1")"
       debugfile="%{install-root}%{debugdir}/${realpath}.debug"
       mkdir -p "$(dirname "$debugfile")"
       objcopy %{objcopy-extract-args} "$1" "$debugfile"
       chmod 644 "$debugfile"
       strip %{strip-args} "$1"
       objcopy %{objcopy-link-args} "$debugfile" "$1"' - {} ';'

environment:
  CPPFLAGS: "-O2 -D_FORTIFY_SOURCE=2"
  CFLAGS: "-O2 -g -fstack-protector-strong"
  CXXFLAGS: "-O2 -g -fstack-protector-strong"
  LDFLAGS: "-fstack-protector-strong -Wl,-z,relro,-z,now"
  LC_ALL: en_US.UTF-8

split-rules:
  devel:
    - "%{includedir}"
    - "%{includedir}/**"
    - "%{libdir}/pkgconfig"
    - "%{libdir}/pkgconfig/**"
    - "%{datadir}/pkgconfig"
    - "%{datadir}/pkgconfig/**"
    - "%{datadir}/aclocal"
    - "%{datadir}/aclocal/**"
    - "%{prefix}/lib/cmake"
    - "%{prefix}/lib/cmake/**"
    - "%{libdir}/cmake"
    - "%{libdir}/cmake/**"
    - "%{prefix}/lib/*.a"
    - "%{libdir}/*.a"

  debug:
    - "%{debugdir}/**"

plugins:
  - origin: local
    path: plugins/elements
    elements:
      flatpak-image: 0

  - origin: pip
    package-name: buildstream-external
    elements:
      x86image: 0

options:
  bootstrap_build_arch:
    type: arch
    description: Architecture
    variable: bootstrap_build_arch
    values:
      - arm
      - aarch64
      - i586
      - x86_64

  target_arch:
    type: arch
    description: Architecture
    variable: arch
    values:
      - arm
      - aarch64
      - i586
      - x86_64

artifacts:
  url: https://cache.sdk.freedesktop.org/artifacts/

elements:
  cmake:
    variables:
      generator: Ninja
  autotools:
    variables:
      remove_libtool_modules: "true"
      remove_libtool_libraries: "true"
      delete_libtool_files: |
          find "%{install-root}" -name "*.la" -print0 | while read -d '' -r file; do
            if grep '^shouldnotlink=yes$' "${file}" &>/dev/null; then
              if %{remove_libtool_modules}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            else
              if %{remove_libtool_libraries}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            fi
          done
      autogen: |
        export NOCONFIGURE=1;
        if [ ! -e configure ]; then
          if [ -e autogen ]; then ./autogen;
          elif [ -e autogen.sh ]; then ./autogen.sh;
          elif [ -e bootstrap ]; then ./bootstrap;
          elif [ -e bootstrap.sh ]; then ./bootstrap.sh;
          else autoreconf -ivf;
          fi
        fi
      conf-global: |
        %{conf-deterministic} \
        %{conf-link-args} \
        %{conf-build} \
        %{conf-host}
      conf-cmd: configure
    config:
      configure-commands:
        - |
          %{autogen}
          if [ -n "%{builddir}" ]; then
            mkdir %{builddir}
            cd %{builddir}
              reldir=..
            else
              reldir=.
          fi
          ${reldir}/%{configure}

      build-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make}

      install-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make-install}

        - |
          %{delete_libtool_files}
